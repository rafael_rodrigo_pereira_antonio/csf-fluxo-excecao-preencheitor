const normalize = (text) => {
    return new String(text).toLocaleLowerCase().replace(' ', '-');
}

module.exports = normalize;