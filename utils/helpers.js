const delay = require('./../utils/delay');

const customBlur = async(page, controlref, timer = 3000) => {
    await delay(500);
    await page.$eval(controlref, e => e.blur());
    await delay(timer);
}

const customBasicIterator = async(page, array,input,controlref,timer ) => {
    for (let index = 0; index < array.length; index++) {
        await input.click();
        await delay(timer);
        await page.keyboard.type(array[index]);
        await customBlur(page, controlref, 2000);
        await input.click({ clickCount: 3 });
        await delay(timer);
        await page.keyboard.press('Delete');
        await delay(1000);
    }
}

module.exports = { customBlur, customBasicIterator };