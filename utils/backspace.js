const backspace = async (page, times) => {
    for (let index = 0; index < times; index++) {
        await page.keyboard.press('Backspace');
    }
}

module.exports = backspace;