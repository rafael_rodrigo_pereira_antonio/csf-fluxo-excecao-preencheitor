var figlet = require('./cli/figlet');
var inquirer = require('inquirer');

var { executarCenarioBase } = require('./cenarios/base');
var { executarCenarioFuncionario } = require('./cenarios/funcionario');
var { executarValidaCpf } = require('./cenarios/cpf');
var { executarValidaCnpj } = require('./cenarios/cnpj');
var { executarValidaEmail } = require('./cenarios/email');
var { executarValidaCelular } = require('./cenarios/celular');
var { executarValidaCep } = require('./cenarios/cep');

var delay = require('./utils/delay');
var normalize = require('./utils/normalize');


let appUrl = 'http://localhost:4200/?data=eyJEYWRvc09wZXJhZG9yIjp7IkNvZGlnb1VzdWFyaW8iOjExNywiVXN1YXJpbyI6ImZlIiwiTm9tZVVzdWFyaW8iOiJGZWxpcGUgTWVuZG9uY2EiLCJUaWNrZXQiOiJ1Mk13ZzdKaGtOc1A4cGs3K2w5K3pHNVR6ZytuOTQ1RFZRdFVzTVZWVU4xYjF3cDVab2srdjFtWXBCMzBkSXRTbzRqNDh2dDA1eHVDKy9JWlBBNXRuakZueUFlVy9wYU9iZGttK1E1OVJSUT0iLCJDb2RpZ29Mb2phU2l0ZWYiOiI4ODg4ODg4ODAzIiwiQ29kaWdvTG9qYUhpZXJhcnF1aWEiOjMyOSwiRGVzY3JpY2FvSGllcmFycXVpYSI6IkxvamFzID4gTWF0cml6ID4gQmFja09mZmljZSBDYXJyZWZvdXIiLCJEYXRhR2VyYWNhbyI6IjIwMjQtMDYtMDZUMTE6NTc6MDMuNTU3MTItMDM6MDAifSwiSWRFbXByZXNhIjoxfQ%3D%3D';

const puppeteer = require('puppeteer');

(async () => {

  figlet.textInicial();

  await delay(500);

  const escolha  = await inquirer.prompt([
    {
        type: 'list',
        name: 'cenario',
        message: 'Qual cenário deseja simlular?',
        choices: ['Base', 'Funcionario', 'Valida CEP' , 'Valida CPF','Valida CNPJ', 'Valida Email', 'Valida Celular'],
    }
]);

  const browser = await puppeteer.launch({ headless: false, args: ['--start-fullscreen'] });
  const page = await browser.newPage();
  await page.setViewport({
  width: 1300,
  height: 600,
  deviceScaleFactor: 1,
});

  const finalizePreencheitor = async() => {
    await browser.close();
    process.exit();
  }

  await page.goto(appUrl);

  if (normalize(escolha.cenario) === 'base') {
    await delay(3000);
    await executarCenarioBase(page);
  }

  if (normalize(escolha.cenario) === 'funcionario') {
    await delay(3000);
    await executarCenarioFuncionario(page);
  }

  if (normalize(escolha.cenario) === 'valida-cpf') {
    await executarValidaCpf(page);
    await finalizePreencheitor();
  }

  if (normalize(escolha.cenario) === 'valida-cnpj') {
    await executarValidaCnpj(page);
    await finalizePreencheitor();
  }

  if (normalize(escolha.cenario) === 'valida-email') {
    await executarValidaEmail(page);
    await finalizePreencheitor();
  }

  if (normalize(escolha.cenario) === 'valida-celular') {
    await executarValidaCelular(page);
    await finalizePreencheitor();
  }

  if (normalize(escolha.cenario) === 'valida-cep') {
    await executarValidaCep(page);
    await finalizePreencheitor();
  }
})();