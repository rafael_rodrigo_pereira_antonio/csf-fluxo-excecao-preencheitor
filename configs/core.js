const BUs = require('../models/BUs');
const Ambientes = require('./../models/Ambientes');

const puppeteerConfigs = {
    // the chromium revision to use
    // default is puppeteer.PUPPETEER_REVISIONS.chromium
    revision: '',

    // additional path to detect local chromium copy (separate with a comma if multiple paths)
    detectionPath: '',

    // custom path to download chromium to local, require dir permission: 0o777
    // default is user home dir
    downloadPath: '',

    // the folder name for chromium snapshots (maybe there are multiple versions)
    folderName: '.chromium-browser-snapshots',

    // the stats file name, cache stats info for latest installation
    statsName: '.pcr-stats.json',

    // default hosts are ['https://storage.googleapis.com', 'https://npmmirror.com/mirrors']
    hosts: [],

    cacheRevisions: 2,
    retry: 3,
    silent: false
};

const defineAppURL = (ambiente, bu) => {

    if (ambiente === Ambientes.DEV) {
        return 'http://localhost:4200/';
    }

    return `https://aquisicao-digital-csf-${bu}-aquisicao-digital-pwa.apps.ocp4${ambiente}.csfcpv.wcorp.carrefour.com/`;

    // if (bu === BUs.ATC) {
    //     return `https://aquisicao-digital-csf-${bu}-aquisicao-digital-pwa.apps.ocp4${ambiente}.csfcpv.wcorp.carrefour.com/`;
    // }

    // if (bu === BUs.CSF) {
    //     return `https://aquisicao-digital-csf-${bu}-aquisicao-digital-pwa.apps.ocp4${ambiente}.csfcpv.wcorp.carrefour.com/`;
    // }

    // if (bu === BUs.SAMS) {
    //     return `https://aquisicao-digital-csf-${bu}-aquisicao-digital-pwa.apps.ocp4${ambiente}.csfcpv.wcorp.carrefour.com/`;
    // }
    // return `https://aquisicao-digital-csf-aquisicao-digital-pwa.apps.ocp4${ambiente}.csfcpv.wcorp.carrefour.com/`;
}


const queryStringdadosOperadorLoja = '?data=eyJDb2RpZ29Vc3VhcmlvIjoxMDE3MiwiVXN1YXJpbyI6InJhZmFlbC5zYW50b3NfIiwiTm9tZVVzdWFyaW8iOiJSYWZhZWwgQXJhdWpvIGRvcyBzYW50b3MiLCJDb2RpZ29Mb2phU2l0ZWYiOiI0MDAxMDA5MDAwIiwiQ29kaWdvTG9qYUhpZXJhcnF1aWEiOjQzODIsIkRlc2NyaWNhb0hpZXJhcnF1aWEiOiJTYW1zIENsdWIgRGlnaXRhbCA%2BIFRFU1RFIC0gU0FNUyIsIkRhdGFHZXJhY2FvIjoiMjAyMi0wOS0yMVQxNToyNzoxNS43NDEzODktMDM6MDAifQ%3D%3D';

module.exports = { puppeteerConfigs, defineAppURL, queryStringdadosOperadorLoja };