const geradorCpf = require('gerador-validador-cpf');
const delay = require('../utils/delay');

const executarCenarioFuncionario = async(page) => {
console.log('-- Início fluxo funcionário --');
await page.focus('#cpf');
  const cpf = geradorCpf.generate();
  await page.keyboard.type(cpf);
  await page.$eval('#cpf', e => e.blur());
  await delay(1000);

  await page.focus('#motivo');
  await page.keyboard.type('Remissão');
  await page.click('.mat-option-text');

  await page.focus('#observacao');
  await page.keyboard.type('Esse campo ser para inserirmos coisas');

  await page.focus('#nome');
  await page.keyboard.type('Funcionario Carrefour');

  await page.focus('#email');
  await page.keyboard.type('funcionario.gomes@gmail.com');

  await page.focus('#celular');
  await page.keyboard.type('11992348800');

  await page.focus('#dataNascimento');
  await page.keyboard.type('11/11/1990');

  await page.click('#genero-option-masc');

  await page.focus('#profissao');
  await page.keyboard.type('Analista de sistemas');
  await page.click('.mat-option-text');

  await page.focus('#renda');
  await page.keyboard.type('3150,50');

  await page.focus('#ocupacao');
  await page.keyboard.type('Funcionário Empresa Privada');
  await page.click('.mat-option-text');

  await page.focus('#nomeMae');
  await page.keyboard.type('Gabrielle Correia Ribeiro');

  await page.click('#pep-option-sim');

  await page.focus('#cep');
  await page.keyboard.type('12345678');
  await page.$eval('#cpf', e => e.blur());
  await delay(1000);

  await page.focus('#numero');
  await page.keyboard.type('12');

  await page.focus('#complemento');
  await page.keyboard.type('Apto 505 bloco 01');

  await page.focus('#produto');
  await page.keyboard.type('VISA HYBRID INTL FUNC');
  await page.click('.mat-option-text');

  await delay(500);

  await page.focus('#limiteCredito');
  await page.keyboard.type('8050.50');

  await page.focus('#formaPagamento');
  await page.keyboard.type('débito total da fatura');
  await page.click('.mat-option-text');

  await page.focus('#dataVencimento');
  await page.keyboard.type('8');
  await page.click('.mat-option-text');
  console.log('-- Fim --');
}

module.exports = { executarCenarioFuncionario };