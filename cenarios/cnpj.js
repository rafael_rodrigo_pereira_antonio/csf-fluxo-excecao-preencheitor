const geradorCnpf = require('node-cnpj');
const delay = require('./../utils/delay');
const backspace = require('./../utils/backspace');

const CONTROL_REF = '#cnpj';

const executarValidaCnpj = async (page) => {
    const input = await page.$('#cnpj');
    let cnpj;

    customBackspace = async() => {
        await input.click();
        await backspace(page, 18);
    }
    
    customBlur = async(timer = 3000) => {
        await page.$eval(CONTROL_REF, e => e.blur());
        await delay(timer);
    }
    

    for (let index = 0; index < 5; index++) {
        cnpj = geradorCnpf.generate();
        await input.click();
        await delay(1000);
        await page.keyboard.type(cnpj);
        await customBlur();
        
        await customBackspace();
        await delay(1000);
        await customBlur(2000);
    }

    await input.click();
    await page.keyboard.type('11111111111111');
    await customBlur(2000);
    await customBackspace();

    await page.keyboard.type('12345678900011');
    await customBlur(2000);
    await customBackspace();
};

module.exports = { executarValidaCnpj };