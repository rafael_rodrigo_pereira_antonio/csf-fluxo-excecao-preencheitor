const geradorCpf = require('gerador-validador-cpf');
const delay = require('./../utils/delay');

const executarCenarioBase = async(page) => {
console.log('-- Início do cenário --> base ...(vulgo fluxo feliz :D) <--');
await page.focus('#cpf');
  const cpf = geradorCpf.generate();
  await page.keyboard.type(cpf);
  await page.$eval('#cpf', e => e.blur());
  await delay(1000);

  await page.focus('#motivo');
  await page.keyboard.type('Remissão');
  await page.click('.mat-option-text');

  await page.focus('#observacao');
  await page.keyboard.type('Esse campo ser para inserirmos coisas');

  await page.focus('#nome');
  await page.keyboard.type('Isabelle Gomes Ribeiro dos Santos');

  await page.focus('#email');
  await page.keyboard.type('isabelle.gomes@gmail.com');

  await page.focus('#celular');
  await page.keyboard.type('11992348800');

  await page.focus('#dataNascimento');
  await page.keyboard.type('12/10/1990');

  await page.click('#genero-option-masc');

  await page.focus('#profissao');
  await page.keyboard.type('Analista de sistemas');
  await page.click('.mat-option-text');

  await page.focus('#renda');
  await page.keyboard.type('3150,50');

  await page.focus('#ocupacao');
  await page.keyboard.type('Funcionário Empresa Privada');
  await page.click('.mat-option-text');

  // await page.focus('#cnpj');
  // await page.keyboard.type('54101888000188');

  await page.focus('#nomeMae');
  await page.keyboard.type('Gabrielle Correia Ribeiro');

  await page.click('#pep-option-sim');

  await page.focus('#cep');
  await page.keyboard.type('12345678');
  await page.$eval('#cpf', e => e.blur());
  await delay(1000);

  // await page.focus('#endereco');
  // await page.keyboard.type('Rua João Pedro de Miranda');

  await page.focus('#numero');
  await page.keyboard.type('12');

  await page.focus('#complemento');
  await page.keyboard.type('Apto 505 bloco 01');

  // await page.focus('#bairro');
  // await page.keyboard.type('Venda Nova');

  // await page.focus('#cidade');
  // await page.keyboard.type('Belo Horizonte');

  // await page.focus('#estado');
  // await page.keyboard.type('Minas');
  // await page.click('.mat-option-text');

  await page.focus('#produto');
  await page.keyboard.type('VS ATACADAO INTL');
  await page.click('.mat-option-text');

  await delay(500);

  await page.focus('#limiteCredito');
  await page.keyboard.type('8050.50');

  await page.focus('#dataVencimento');
  await page.keyboard.type('1');
  await page.click('.mat-option-text');
  console.log('-- Fim --');
}

module.exports = { executarCenarioBase };