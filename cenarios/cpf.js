const geradorCpf = require('gerador-validador-cpf');
const delay = require('./../utils/delay');
const backspace = require('./../utils/backspace');

const executarValidaCpf = async (page) => {
    console.log('-- Início do cenário --> valida CPF <--');
    
    const input = await page.$('#cpf');
    console.log('# Geração de CPFs válidos dinâmicos #');
    for (let index = 0; index < 5; index++) {
        await delay(500);
        await input.click();
        await page.keyboard.type(geradorCpf.generate());
        await delay(500);
        await page.$eval('#cpf', e => e.blur());
        await delay(500);
        await input.click();
        await backspace(page, 14);
    }

    console.log('# Validação de CPFs inválidos #');

    await delay(1000);
    await input.click();
    await page.keyboard.type('11111111111');
    await delay(1000);
    
    await backspace(page, 14);
    
    await delay(1000);
    await page.$eval('#cpf', e => e.blur());
    await input.click();
    await page.keyboard.type('12312312300');

    await delay(3000);
    console.log('-- Cenário execudado com sucesso!');
    console.log('-- Fechando navegador... --');
}

module.exports = { executarValidaCpf };