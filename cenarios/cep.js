const delay = require('./../utils/delay');
const helpers = require('./../utils/helpers');

const CONTROL_REF = '#cep';
const DELAY_DEFAULT = 500;

const executarValidaCep = async (page) => {
    const input = await page.$(CONTROL_REF);

    const array = [
        '86060-310',
        '64001-050',
        '08472-301',
        '00000-000',
        '55555',
        '77777-77'
    ];

    await helpers.customBasicIterator(page, array, input , CONTROL_REF, DELAY_DEFAULT);
    await delay(1000);
};

module.exports = { executarValidaCep };