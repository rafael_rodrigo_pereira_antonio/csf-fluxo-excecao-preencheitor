const delay = require('./../utils/delay');
const helpers = require('./../utils/helpers');

const CONTROL_REF = '#celular';
const DELAY_DEFAULT = 500;

const executarValidaCelular = async (page) => {
    const input = await page.$(CONTROL_REF);

    const array = [
        '(81) 91248-5989',
        '(96) 99259-6904',
        '(27) 99321-6444',
        '(11) 86082-5535',
        '(11) 57782-5532',
        '(11) 11111-1111'
    ];

    await helpers.customBasicIterator(page, array, input , CONTROL_REF, DELAY_DEFAULT);
    await delay(1000);
    // for (let index = 0; index < array.length; index++) {
    //     await input.click();
    //     await delay(DELAY_DEFAULT);
    //     await page.keyboard.type(array[index]);
    //     await helpers.customBlur(page, CONTROL_REF, 2000);
    //     await input.click({ clickCount: 3 });
    //     await delay(DELAY_DEFAULT);
    //     await page.keyboard.press('Delete');
    //     await delay(1000);
    // }
};

module.exports = { executarValidaCelular };