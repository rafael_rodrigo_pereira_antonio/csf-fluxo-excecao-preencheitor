const delay = require('./../utils/delay');
const helpers = require('./../utils/helpers');

const CONTROL_REF = '#email';
const DELAY_DEFAULT = 500;

const executarValidaEmail = async (page) => {
    const input = await page.$(CONTROL_REF);
    
    const array = [
        'isadoraa.braga01@yahoo.com', 
        'helosacarvalho90@live.com', 
        'helosacarvalho90@live.com', 
        'fakemail.com', 
        'ba@.com.br', 
        'fakema$il@.com'
    ];

    for (let index = 0; index < array.length; index++) {
        await input.click();
        await delay(DELAY_DEFAULT);
        await page.keyboard.type(array[index]);
        await helpers.customBlur(page, CONTROL_REF, 1000);
        await input.click({ clickCount: 3 });
        await delay(DELAY_DEFAULT);
        await page.keyboard.press('Delete');
        await delay(DELAY_DEFAULT);
    }
};

module.exports = { executarValidaEmail };