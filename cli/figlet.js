const figlet = require('figlet');

const textInicial = () => {
    figlet.text(
        "Preencheitor EX",
        {
          horizontalLayout: "default",
          verticalLayout: "default",
          width: 100,
          whitespaceBreak: true,
        },
        function (err, data) {
          if (err) {
            console.log("Something went wrong...");
            return;
          }
          console.log(data);
        }
      );
}

module.exports = { textInicial };